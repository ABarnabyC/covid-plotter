//const BundleAnalyzerPlugin = require("webpack-bundle-analyzer")
//  .BundleAnalyzerPlugin;

module.exports = {
  publicPath: process.env.NODE_ENV === "production" ? "/covid-plotter/" : "/",
  transpileDependencies: ["vuetify"],
  //configureWebpack: {
  //  plugins: [new BundleAnalyzerPlugin()],
  //},
  chainWebpack: (config) => {
    config.plugin("VuetifyLoaderPlugin").tap((args) => [{
      match(originalTag, {
        kebabTag,
        camelTag,
        path,
        component
      }) {
        if (kebabTag.startsWith("core-")) {
          return [
            camelTag,
            `import ${camelTag} from '@/components/core/${camelTag.substring(
                4
              )}.vue'`,
          ];
        }
      },
    }, ]);
  },
};