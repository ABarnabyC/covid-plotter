export default {
    mean(data, column) {
        let total = 0;
        const mult = 1.0 / data.length;
        for (let i = 0; i < data.length; i++) {
            total = total + (data[i][column] * mult);
        }
        return total;
    },

    variance(data, column) {
        const mean = this.mean(data, column);
        const mult = 1.0 / data.length;
        let total = 0;
        for (let i = 0; i < data.length; i++) {
            let delta = data[i][column] - mean;
            total = total + (delta * delta * mult);
        }
        return total;
    },

    stdDev(data, column) {
        return Math.sqrt(this.variance(data, column));
    },

    max(data, column) {
        let maxVal = data[0][column];
        for (let i = 1; i < data.length; i++) {
            if (maxVal < data[i][column]) maxVal = data[i][column];
        }
        return maxVal;
    },

    min(data, column) {
        let minVal = data[0][column];
        for (let i = 1; i < data.length; i++) {
            if (minVal > data[i][column]) minVal = data[i][column];
        }
        return minVal;
    }
}