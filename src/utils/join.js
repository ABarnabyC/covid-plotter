import County from "./../store/county";

function createLookup(lookupTable, lookupKeyCreater) {
  let lookupIndex = [];
  for (let i = 0; i < lookupTable.length; i++) {
    let row = lookupTable[i];
    lookupIndex[lookupKeyCreater(row)] = row;
  }
  return lookupIndex;
}

function createCounty(county, state, stats) {
  const tempCounty = new County();
  tempCounty.id = stats["state"] + stats["county"];
  tempCounty.name = county["Area Name"]
    .replace(" County", "")
    .replace(" Parish", "")
    .replace(" Borough", "")
    .replace(" city", "");
  tempCounty.stateName = state["Area Name"];
  tempCounty.population = stats.pop;
  tempCounty.density = stats.density;
  return tempCounty;
}

function handleNYC(NYC, county, state, stats) {
  if (NYC == null) {
    NYC = createCounty(county, state, stats);
  } else if (county["Area Name"] == "New York County") {
    NYC.id = stats["state"] + stats["county"];
    NYC.density = addDensities(NYC.density, NYC.population, stats.density, stats.pop);
    NYC.population += stats.pop;
  } else {
    NYC.density = addDensities(NYC.density, NYC.population, stats.density, stats.pop);
    NYC.population += stats.pop;
  }

  NYC.name = "New York";

  return NYC;
}

function addDensities(densityA, populationA, densityB, populationB) {
  return (populationA + populationB) / ((populationA / densityA) + (populationB / densityB));
}

export default {
  singleKey(lookupTable, mainTable, lookupKey, mainKey, select) {
    var m = mainTable.length,
      output = [];
    let lookupIndex = createLookup(lookupTable, (r) => {
      return r[lookupKey];
    });
    for (var j = 0; j < m; j++) {
      // loop through m items
      var y = mainTable[j];
      var x = lookupIndex[y[mainKey]]; // get corresponding row from lookupTable
      output.push(select(y, x)); // select only the columns you need
    }
    return output;
  },

  splitKey(lookupTable, mainTable, lookupKey, mainKey, select) {
    var l = lookupTable.length,
      m = mainTable.length,
      lookupIndex = [],
      output = [];
    for (var i = 0; i < l; i++) {
      // loop through l items
      var row = lookupTable[i];
      lookupIndex[row[lookupKey[0]] + "_" + row[lookupKey[1]]] = row; // create an index for lookup table
    }
    for (var j = 0; j < m; j++) {
      // loop through m items
      var y = mainTable[j];
      var x = lookupIndex[y[mainKey[0]] + "_" + y[mainKey[1]]]; // get corresponding row from lookupTable
      output.push(select(y, x)); // select only the columns you need
    }
    return output;
  },

  stateCounties(lookupTable, mainTable) {
    var m = mainTable.length,
      output = [];
    let lookupIndex = createLookup(lookupTable, (c) => {
      return c["State Code"] + c["County Code"];
    });

    let NYC = null;
    const NYCCounties = [
      "Kings County",
      "Queens County",
      "New York County",
      "Bronx County",
      "Richmond County",
    ];

    for (var j = 0; j < m; j++) {
      const y = mainTable[j];
      // Skip puerto rico
      if (y["state"] == "72") continue;

      const county = lookupIndex[y["state"] + y["county"]]; // get corresponding row from lookupTable
      const state = lookupIndex[y["state"] + "000"]; // get corresponding row from lookupTable

      if (state["Area Name"] == "New York" && NYCCounties.includes(county["Area Name"])) {
        NYC = handleNYC(NYC, county, state, y);
      } else {
        output.push(createCounty(county, state, y));
      }
    }
    output.push(NYC);
    return output;
  },

  countyCovid(countyTable, covidTable, deathTable) {
    const m = countyTable.length,
      dailyUpdatesIndex = [];

    const lookupIndex = createLookup(covidTable, (c) => {
      return c["Province_State"] + "_" + c["Admin2"];
    });

    for (let [key] of Object.entries(covidTable[0])) {
      //Create the index of the daily update columns and ensure its sorted
      if (key.includes("/")) {
        dailyUpdatesIndex.push({
          key: key,
          date: Date.parse(key),
        });
      }
    }
    dailyUpdatesIndex.sort((a, b) => {
      if (a.date < b.date) return -1;
      else if (a.date == b.date) return 0;
      else return 1;
    });

    const deathIndex = createLookup(deathTable, (c) => {
      return c["Province_State"] + "_" + c["Admin2"]
    });

    let first = true;
    const US = new County();
    US.id = "US";
    US.name = "US";
    US.stateName = "";

    const USwoNYC = new County();
    USwoNYC.id = "USwoNYC";
    USwoNYC.name = "US without NYC";
    USwoNYC.stateName = "";

    const NYArea = [
      "Kings_New York",
      "Queens_New York",
      "New York_New York",
      "Bronx_New York",
      "Richmond_New York",
      "Westchester_New York",
      "Bergen_New Jersey",
      "Hudson_New Jersey",
      "Passaic_New Jersey",
      "Putnam_New York",
      "Rockland_New York",
    ];

    for (let i = 0; i < m; i++) {
      // loop through m items
      const y = countyTable[i];
      const cases = lookupIndex[y.stateName + "_" + y.name];
      const deaths = deathIndex[y.stateName + "_" + y.name];
      if (cases == null) {
        console.log(y.stateName + "_" + y.name);
      } else {
        let dailyUpdates = [];
        let firstCase = false;
        for (let j = 0; j < dailyUpdatesIndex.length; j++) {
          let caseCount = parseFloat(cases[dailyUpdatesIndex[j].key]);
          let deathCount = parseFloat(deaths[dailyUpdatesIndex[j].key]);
          if (caseCount > 0) firstCase = true;

          //Add up the cases for the US 
          if (first) {
            US.population = y.population;
            US.dailyUpdates.push({
              date: dailyUpdatesIndex[j].date,
              cases: caseCount,
              deaths: deathCount,
            });
            USwoNYC.population = y.population;
            USwoNYC.dailyUpdates.push({
              date: dailyUpdatesIndex[j].date,
              cases: caseCount,
              deaths: deathCount,
            });
          } else {
            US.population += y.population;
            US.dailyUpdates[j].cases += caseCount;
            US.dailyUpdates[j].deaths += deathCount;
            if (!NYArea.includes(y.name + "_" + y.stateName)) {
              USwoNYC.population += y.population;
              USwoNYC.dailyUpdates[j].cases += caseCount;
              USwoNYC.dailyUpdates[j].deaths += deathCount;
            }
          }

          //wait till we have seen a case to start adding to the history
          if (firstCase) {
            dailyUpdates.push({
              date: dailyUpdatesIndex[j].date,
              cases: caseCount,
              deaths: deathCount,
            });
          }
        }
        first = false;

        y.dailyUpdates = dailyUpdates;
      }
    }
    return [US, USwoNYC];

  },
};