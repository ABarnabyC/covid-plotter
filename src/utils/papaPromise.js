import Papa from "papaparse";

export function papaPromise(importFile) {
    return new Promise((resolve, reject) => {
        Papa.parse(importFile, {
            download: true,
            dynamicTyping: false,
            header: true,
            complete: function (results) {
                resolve(results);
            },
            error: function (error) {
                reject(error);
            },
        });
    });
}