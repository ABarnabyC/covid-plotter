export default class County {
    constructor() {
        this.name = "";
        this.stateName = "";
        this.id = "";
        this.population = 1;
        this.density = 0;
        this.dailyUpdates = [];
    }

    normalize(toNormalize) {
        return (toNormalize / this.population) * 10000;
    }

    normalizeHist(toNormalize) {
        let normalized = [];
        for (let i = 0; i < toNormalize.length; i++) {
            normalized[i] = [toNormalize[i][0], this.normalize(toNormalize[i][1])];
        }
        return normalized;
    }

    get PositiveHist() {
        let hist = [];
        for (let i = 0; i < this.dailyUpdates.length; i++) {
            hist[i] = [this.dailyUpdates[i].date, this.dailyUpdates[i].cases];
        }
        return hist;
    }

    get PositiveHistPer10k() {
        return this.normalizeHist(this.PositiveHist);
    }

    get Avg7DayCaseHist() {
        let hist = [];
        for (let i = 7; i < this.dailyUpdates.length; i++) {
            hist[i - 7] = [
                this.dailyUpdates[i].date,
                Math.max(this.dailyUpdates[i].cases - this.dailyUpdates[i - 7].cases, 0.0) / 7.0,
            ];
        }
        return hist;
    }

    get Avg7DayCaseHistPer10k() {
        return this.normalizeHist(this.Avg7DayCaseHist);
    }

    get DeathHist() {
        let hist = [];
        for (let i = 0; i < this.dailyUpdates.length; i++) {
            hist[i] = [this.dailyUpdates[i].date, this.dailyUpdates[i].deaths];
        }
        return hist;
    }

    get DeathHistPer10k() {
        return this.normalizeHist(this.DeathHist);
    }

    get SevenDayDeathHist() {
        let hist = [];
        for (let i = 7; i < this.dailyUpdates.length; i++) {
            hist[i - 7] = [
                this.dailyUpdates[i].date,
                Math.max(this.dailyUpdates[i].deaths - this.dailyUpdates[i - 7].deaths, 0.0),
            ];
        }
        return hist;
    }

    get SevenDayDeathHistPer10k() {
        return this.normalizeHist(this.SevenDayDeathHist);
    }

    get Positives() {
        if (this.dailyUpdates.length > 0)
            return this.dailyUpdates[this.dailyUpdates.length - 1].cases;
        else return 0;
    }
    get PositivesPer10k() {
        return this.normalize(this.Positives);
    }
    get Avg7DayCases() {
        if (this.dailyUpdates.length > 7)
            return Math.max(this.Positives -
                    this.dailyUpdates[this.dailyUpdates.length - 8].cases, 0.0) /
                7.0;
        else return 0;
    }
    get Avg7DayCasesPer10k() {
        return this.normalize(this.Avg7DayCases);
    }

    get PercentChange7DayCases() {
        if (this.dailyUpdates.length > 14) {
            const old = (this.dailyUpdates[this.dailyUpdates.length - 8].cases -
                this.dailyUpdates[this.dailyUpdates.length - 15].cases) / 7.0;
            if (old < 1) return 0.0;
            else return (this.Avg7DayCases - old) / old * 100.0;
        } else return 0;
    }
    get Change7DayCases() {
        if (this.dailyUpdates.length > 14)
            return (
                (this.Positives +
                    this.dailyUpdates[this.dailyUpdates.length - 15].cases -
                    2.0 * this.dailyUpdates[this.dailyUpdates.length - 8].cases) /
                7.0
            );
        else return 0;
    }
    get Change7DayCasesPer10k() {
        return this.normalize(this.Change7DayCases);
    }

    get Deaths() {
        if (this.dailyUpdates.length > 0)
            return this.dailyUpdates[this.dailyUpdates.length - 1].deaths;
        else return 0;
    }
    get DeathsPer10k() {
        return this.normalize(this.Deaths);
    }
    get SevenDayDeaths() {
        if (this.dailyUpdates.length > 7)
            return Math.max(this.Deaths - this.dailyUpdates[this.dailyUpdates.length - 8].deaths, 0.0);
        else return 0;
    }
    get SevenDayDeathsPer10k() {
        return this.normalize(this.SevenDayDeaths);
    }
}