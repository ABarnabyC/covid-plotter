// store.js
import Vue from "vue";
import Vuex from "vuex";

import {
  papaPromise
} from "./../utils/papaPromise";
import Axios from "axios";
import join from "./../utils/join";

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    counties: [],
    selected: [],
    loading: true,
    USvsNY: [],
  },
  actions: {
    async loadCountyData({
      commit
    }) {
      const confirmedPromise = papaPromise(
        "https://raw.githubusercontent.com/CSSEGISandData/COVID-19/master/csse_covid_19_data/csse_covid_19_time_series/time_series_covid19_confirmed_US.csv"
      );
      const deathsPromise = papaPromise(
        "https://raw.githubusercontent.com/CSSEGISandData/COVID-19/master/csse_covid_19_data/csse_covid_19_time_series/time_series_covid19_deaths_US.csv"
      );
      const geocodePromise = papaPromise(
        "https://abarnabyc.gitlab.io/covid-plotter/assets/geocodes.csv"
      );
      const popPromise = Axios({
        method: "get",
        url: "https://abarnabyc.gitlab.io/covid-plotter/assets/population.json",
        responseType: "json",
        transformResponse: [
          function (data) {
            let transformedData = [];
            for (let i = 1; i < data.length; i++) {
              transformedData.push({
                pop: parseInt(data[i][0]),
                density: parseFloat(data[i][1]),
                state: data[i][2],
                county: data[i][3],
              });
            }

            return transformedData;
          },
        ],
      });


      const counties = join.stateCounties(
        (await geocodePromise).data,
        (await popPromise).data
      );

      const usvsNYC = join.countyCovid(
        counties,
        (await confirmedPromise).data,
        (await deathsPromise).data
      );

      commit("setCounties", counties);
      commit("setUSvsNY", usvsNYC);
      commit("setLoading", false);
    },
  },
  //getters{},
  mutations: {
    setCounties(state, payload) {
      state.counties = payload;
    },
    setSelected(state, payload) {
      state.selected = payload;
    },
    setLoading(state, payload) {
      state.loading = payload;
    },
    setUSvsNY(state, payload) {
      state.USvsNY = payload;
    },
  },
});