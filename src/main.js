import Vue from "vue";
import App from "./App";
import Vuetify from "vuetify/lib";
import "vuetify/dist/vuetify.min.css";
import vuetify from "./plugins/vuetify";
import "roboto-fontface/css/roboto/roboto-fontface.css";
import HighchartsVue from "highcharts-vue";
import Highcharts from 'highcharts';
import Axios from 'axios';
import mapInit from 'highcharts/modules/map';
import store from "./store/store";

mapInit(Highcharts)
Axios({
  method: "get",
  url: "https://abarnabyc.gitlab.io/covid-plotter/assets/US-All-All.json",
  responseType: "json"
}).then(mapData => {
  Highcharts.maps["myMapName"] = mapData.data;
});

Vue.use(Vuetify);
Vue.use(HighchartsVue);

Vue.config.productionTip = false;

/* eslint-disable no-new */
new Vue({
  el: "#app",
  vuetify,
  store: store,
  render: (h) => h(App),

  components: {
    App,
  },
});